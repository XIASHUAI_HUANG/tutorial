package Hwgithup;

/**
 * Created by huangxiashuai on 12/14/14.
 */
import java.util.List;


import org.joda.time.DateTime;

public class EuropeanCall implements Payout {


    private double K;
    private int N;//N is number of days
    private double rate;

    public EuropeanCall(double K, int N, double rate)
    {
        this.K = K;
        this.N = N;
        this.rate = rate;
    }

    @Override
    public double getPayout(StockPath path) {

        List<Pair<DateTime, Double>> prices = path.getPrices();
        //Calculate the payout
        double FV =Math.max(0,prices.get(prices.size()-1).getValue() - K);
        double PV =FV*Math.exp(-1*N*rate); //discount the future value to the present value
        return PV;
    }
}



