package Hwgithup;

/**
 * Created by huangxiashuai on 12/14/14.
 */
public class Pair<K, V> {

    private K k;
    private V v;

    public Pair(K key, V value){
        k = key;
        v = value;
    }

    public K getKey()
    {
        return k;
    }

    public V getValue()
    {
        return v;
    }

    public void setKey(K  newKey){
        this.k = newKey;
    }
    public void setValue(V newValue){
        this.v= newValue;
    }

    //change the data to String type
    @Override
    public String toString(){
        String str = "";
        str += this.k.toString();
        str += " ";
        str += this.v.toString();
        return str;
    }


}
