package Hwgithup;

/**
 * Created by huangxiashuai on 12/14/14.
 */
import java.util.List;


import org.joda.time.DateTime;

public class AsianCall implements Payout {

    private double K;
    private int N;
    private double rate;

    public AsianCall(double K, int N, double rate)
    {
        this.K = K;
        this.N = N;//N is the number of days
        this.rate = rate;
    }

    @Override
    public double getPayout(StockPath path) {

        List<Pair<DateTime, Double>> prices = path.getPrices();
        //Calculate the payout
        double sum = 0.0;//to help calculate the average price
        for(Pair<DateTime, Double> price : prices)
        {
            sum += (double) price.getValue();
        }

        double average = sum/prices.size();


        double FV =Math.max(0, average - K);
        double PV =FV*Math.exp(-1*N*rate); //discount the future value to the present value
        return PV;

    }

}

