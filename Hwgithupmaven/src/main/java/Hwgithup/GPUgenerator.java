package Hwgithup;

/**
 * Created by huangxiashuai on 12/14/14.
 */
import com.nativelibs4java.opencl.*;
import org.bridj.Pointer;

import java.util.Random;

import static org.bridj.Pointer.allocateFloats;


public class GPUgenerator implements RandomVectorGenerator{

    private int N;// length of random vector

    private int batchNum;

    private double[] normalSequence;// store the normal random vector in GPU

    private int idx;// indicate if we need to regenerate the normal random variable

    /*
    * The constructor of GPU Generator
    * */
    public GPUgenerator(int N, int batchNum){
        this.N = N;
        this.batchNum = batchNum;
        normalSequence = getGPUGaussian(this.batchNum);
        idx = 0;
    }

    @Override

    //This method will generate the normal random variable with length N
    public double[] getVector() {
        double[] vector = new double[N];
        for(int i = 0; i < N; i++){
            // if the idx reaches the generated amount of normal random number
            if(idx == 2*batchNum - 1) {
                normalSequence = getGPUGaussian(this.batchNum);
            }
            //  assign the generated normal to the vector
            vector[i] = normalSequence[idx];
            idx++;
        }
        return vector;
    }


    //This method will generate normal random variable and it uses the best device chosen by kernal

    public double[] getGPUGaussian(int batchNum){

        idx = 0;

        CLPlatform clPlatform = JavaCL.listPlatforms()[0];

        CLDevice device = clPlatform.listAllDevices(true)[0];

        CLContext context = JavaCL.createContext(null, device);

        CLQueue queue = context.createDefaultQueue();
        String src = "__kernel void fill_in_values(__global const float* a, __global const float* b, __global float* out1, __global float* out2, float pi, int n) \n" +
                "{\n" +
                "    int i = get_global_id(0);\n" +
                "    if (i >= n)\n" +
                "        return;\n" +
                "\n" +
                "    out1[i] = sqrt(-2*log(a[i]))*cos(2*pi*b[i]);\n" +
                "    out2[i] = sqrt(-2*log(a[i]))*sin(2*pi*b[i]);\n" +
                "}";
        CLProgram program = context.createProgram(src);
        program.build();

        CLKernel kernel = program.createKernel("fill_in_values");

        final int n = batchNum;
        final Pointer<Float>
                aPtr = allocateFloats(n),
                bPtr = allocateFloats(n);
        // record the uniform vector in order for GPU
        double[] uniformSequence = (new UniformRandomGenerator(2*batchNum)).getVector();

        for (int i = 0; i < n; i++) {
            // generate uniform sequence and assign it to aPtr and bPtr
            aPtr.set(i, (float) uniformSequence[2*i]);
            bPtr.set(i, (float) uniformSequence[2*i+1]);
        }


        CLBuffer<Float>// Create OpenCL input buffers
                a = context.createFloatBuffer(CLMem.Usage.Input, aPtr),
                b = context.createFloatBuffer(CLMem.Usage.Input, bPtr);

        // Create an OpenCL output buffer :
        CLBuffer<Float>
                out1 = context.createFloatBuffer(CLMem.Usage.Output, n),
                out2 = context.createFloatBuffer(CLMem.Usage.Output, n);

        kernel.setArgs(a, b, out1, out2, (float) Math.PI, batchNum);
        CLEvent event = kernel.enqueueNDRange(queue, new int[]{n}, new int[]{128});
        event.invokeUponCompletion(new Runnable() {
            @Override
            public void run() {

            }
        });
        final Pointer<Float> c1Ptr = out1.read(queue,event);
        final Pointer<Float> c2Ptr = out2.read(queue,event);

        double[] normalSequence = new double[2*batchNum];
        for(int i = 0; i < batchNum; i++){
            normalSequence[2*i] = (double) c1Ptr.get(i);
            normalSequence[2*i+1] = (double) c2Ptr.get(i);
        }
        return normalSequence;
    }

}
