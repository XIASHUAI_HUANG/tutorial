package Hwgithup;

/**
 * Created by huangxiashuai on 12/14/14.
 */
public class SimulationManager {

    private StockPath stockPath;
    private Payout payout;

    public SimulationManager(StockPath stockPath, Payout payout){
        this.stockPath = stockPath;
        this.payout = payout;
    }

    public double getPrice(){


        Stats statsRecord = new Stats();
        //use the Stats to record the data and calculate the mena, variance and the number of elements

        double tempPayout = 0.0;

        while(true){

            tempPayout = payout.getPayout(stockPath);
            statsRecord.addStats(tempPayout);

            double sigmaSquare = statsRecord.getVar();

            //break the loop if it satisfy the condition
            if(sigmaSquare !=0 && (double)9*sigmaSquare/0.0001 < statsRecord.getN()) break;


        };

        //System.out.println("the average of payout is " + statsRecord.getMean());
        //System.out.println("the number of payout is " + statsRecord.getN());
        return statsRecord.getMean();

    }
}
