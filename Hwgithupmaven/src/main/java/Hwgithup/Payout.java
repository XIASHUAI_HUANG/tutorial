package Hwgithup;

/**
 * Created by huangxiashuai on 12/14/14.
 */
//This interface is for calculate the payout

public interface Payout {

    public double getPayout(StockPath path);
}

