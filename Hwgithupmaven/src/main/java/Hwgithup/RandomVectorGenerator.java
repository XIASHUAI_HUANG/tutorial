package Hwgithup;

/**
 * Created by huangxiashuai on 12/14/14.
 */
//This interface is for Random vector generator

public interface RandomVectorGenerator {

    public double[] getVector();
}
