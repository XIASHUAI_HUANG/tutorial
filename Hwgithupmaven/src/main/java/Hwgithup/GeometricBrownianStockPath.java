package Hwgithup;

/**
 * Created by huangxiashuai on 12/14/14.
 */
import java.util.LinkedList;


import java.util.List;

import org.joda.time.DateTime;


// This class implements the stock path interface
// It  generates a stock bath based on Geometric Brownian motion

public class GeometricBrownianStockPath implements StockPath {

    private double rate;
    private double sigma;
    private double S0;
    private int N;
    private DateTime startDate;
    private DateTime endDate;
    private RandomVectorGenerator rvg;

    public  GeometricBrownianStockPath(double rate, double sigma, double S0, int N, DateTime startDate, DateTime endDate, RandomVectorGenerator rvg)
    {
        this.rate = rate;
        this.S0 = S0;
        this.sigma = sigma;
        this.N = N;
        this.rvg = rvg;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    @Override
    public List<Pair<DateTime, Double>> getPrices() {

        double[] rv= rvg.getVector();
        DateTime current = new DateTime(startDate.getMillis());

        //calculate the delta per day and delta root per day
        long delta = (endDate.getMillis() - startDate.getMillis())/N;
        double deltaDay = (double) delta/86400000;
        double deltaRootDay = Math.sqrt(deltaDay);

        List<Pair<DateTime, Double>> path = new LinkedList<Pair<DateTime,Double>>();
        path.add(new Pair<DateTime, Double>(current, S0));

        //generate the path of stock prices
        for ( int i=1; i < N; ++i)
        {
            current.plusMillis((int) delta);
            path.add(new Pair<DateTime, Double>(current,
                    path.get(path.size()-1).getValue()*Math.exp((rate-sigma*sigma/2)*deltaDay+sigma * rv[i-1]*deltaRootDay)));
        }

        return path;
    }

}
