package Hwgithup;

/**
 * Created by huangxiashuai on 12/14/14.
 */
import java.util.Random;

public class GaussianRandomVectorGenerator implements RandomVectorGenerator {

    private int N;
    private double miu;
    private double sigma;

    public GaussianRandomVectorGenerator(int N, double miu, double sigma) {
        this.N = N;
        this.miu = miu;
        this.sigma = sigma;
    }

    @Override
    public double[] getVector() {
        Random GaussianRandom = new Random();
        double[] GuassianRandomvector = new double[N];

        //To generate N Gaussian random variables with mean miu and standard variance sigma
        //and then assign them to the GuassianRandomVector
        for (int i = 0; i < GuassianRandomvector.length; ++i) {
            GuassianRandomvector[i] = GaussianRandom.nextGaussian() * sigma + miu;
        }

        return GuassianRandomvector;
    }
}

