package Hwgithup;

/**
 * Created by huangxiashuai on 12/14/14.
 */
// This is class to compute mean of payout and its variance

public class Stats {
    private double sumX;
    private double sumXX;
    private double var;
    private int _N;

    //initialize the stats
    public Stats() {
        sumX = 0.0;
        sumXX = 0.0;
        var = 0.0;
        _N = 0;
    }

    //read new data and calculate the sum and sum of square and number of elements
    public void addStats(double newData) {
        sumX = sumX + newData;
        sumXX = sumXX + newData * newData;
        _N++;
    }

    //to calculate and return the mean value
    public double getMean() {
        return (sumX / _N);
    }
    // to calculate and return the variance
    public double getVar() {

        var = (sumXX / _N) - (sumX / _N) * (sumX / _N);
        return var;
    }

    // return number of elements in the stock path
    public int getN() {
        return _N;
    }
}

