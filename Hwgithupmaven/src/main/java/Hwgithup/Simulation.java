package Hwgithup;

/**
 * Created by huangxiashuai on 12/14/14.
 */
import org.joda.time.DateTime;

public class Simulation {

    public static void main(String[] args)
    {
        int N=1000;
        int batch = 1024*1024;

        DateTime start = new DateTime(2014,10,16,0,0);
        DateTime end = start.plusDays(252);

        GPUgenerator gpu = new GPUgenerator(N, batch);
        StockPath gbsp=new GeometricBrownianStockPath(0.0001, 0.01, 152.35, N, start, end, gpu);


        //calculate the European call option price with strike price 165
        EuropeanCall euroCall = new EuropeanCall (165, 252, 0.0001);
        SimulationManager manager1 = new SimulationManager(gbsp, euroCall);
        double ECprice = manager1.getPrice();
        System.out.println("the European call option price is "+ ECprice);


        //calculate the Asian call option price with strike price 164
        AsianCall asianCall = new AsianCall (164,252,0.0001);
        SimulationManager manager2 = new SimulationManager(gbsp, asianCall);
        double ACprice = manager2.getPrice();
        System.out.println("the Asian call option price is "+ ACprice);

    }
}
