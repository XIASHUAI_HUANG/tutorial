package Hwgithup;

/**
 * Created by huangxiashuai on 12/14/14.
 */

import java.util.Random;

public class UniformRandomGenerator implements RandomVectorGenerator{
    private int N;

    public UniformRandomGenerator(int N){
        this.N = N;
    }


    //This method will generate  uniform double vectors which range from 0 to 1


    public double[] getVector(){
        Random r = new Random();
        double[] vector = new double[N];
        for ( int i = 0; i < vector.length; ++i){
            vector[i] = r.nextDouble();
        }
        return vector;
    }

}
