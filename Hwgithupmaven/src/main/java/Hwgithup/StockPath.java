package Hwgithup;

/**
 * Created by huangxiashuai on 12/14/14.
 */
import java.util.List;


import org.joda.time.DateTime;

//This interface is for creating StockPath.
//The returned list should be ordered by date.

public interface StockPath {
    public List<Pair<DateTime,Double>> getPrices();
}

